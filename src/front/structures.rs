use std::char;
use std::ops::{Add,Sub};
use std::path::PathBuf;
use std::fmt::Debug;

use ncurses::*;
use unicode_segmentation::UnicodeSegmentation;

use back::{*};
use front::colors::{*};

pub fn truncate(string : &String, limit : usize) -> String {
    let title_str = &string[..];
    let title_vec = UnicodeSegmentation::graphemes(title_str, true).collect::<Vec<&str>>();
    if title_vec.len() >= limit {
        let mut trunc = title_vec[..(limit-1)].join("");
        trunc.push_str("~");
        trunc
    }
    else {
        title_vec.join("")
    }
}

pub fn screen_size() -> YX {
    let mut max_y = 0;
    let mut max_x = 0;
    getmaxyx(stdscr(), &mut max_y, &mut max_x);
    YX(max_y,max_x)
}

#[derive(Copy,Clone,Debug)]
pub struct YX(pub i32, pub i32);
impl YX {
    pub fn from_u(y: usize, x: usize) -> YX {
        YX(y as i32, x as i32)
    }
    pub fn from_i32(y: i32, x: i32) -> YX {
        YX(y, x)
    }
}

impl Add for YX {
    type Output = YX;
    fn add(self, rhs: YX) -> YX {
        YX( self.0 + rhs.0,
            self.1 + rhs.1
        )
    }
}

impl Sub for YX {
    type Output = YX;
    fn sub(self, rhs: YX) -> YX {
        YX( self.0 - rhs.0,
            self.1 - rhs.1
        )
    }
}

/// A structure to paginate the Entry vector
#[derive(Debug)]
pub struct EntryView {
    entries: Vec<Entry>,
    start: usize,
    end: usize,
    count: usize
}

impl EntryView {
    pub fn new<'a>(entries: &'a [Entry], start: usize, end: usize) -> EntryView {
        EntryView {
            entries: Vec::from(entries),
            start,
            end,
            count: start
        }
    }

    pub fn len(&self) -> usize {
        self.entries.len()
    }
}

// CONSUMING iterator
impl Iterator for EntryView {
    type Item = Entry;
    fn next(&mut self) -> Option<Self::Item> {
        if self.count < self.end && self.count < self.entries.len()  {
            let next = self.count;
            let out = self.entries.remove(next);
            self.count += 1;
            Some(out)
        }
        else {
            None
        }
    }
}

// NON-CONSUMING iterator
pub struct EntryViewIterator<'a> {
    ev: &'a EntryView,
    count: usize
}

impl<'a> Iterator for EntryViewIterator<'a> {
    type Item = &'a Entry;
    fn next(&mut self) -> Option<Self::Item> {
        if self.count < self.ev.end && self.count < self.ev.entries.len()  {
            let next = self.count;
            self.count += 1;
            Some(&self.ev.entries[next])
        }
        else {
            None
        }
    }
}

impl<'a> IntoIterator for &'a EntryView {
        type Item = &'a Entry;
        type IntoIter = EntryViewIterator<'a>;
        fn into_iter(self) -> Self::IntoIter {
            EntryViewIterator { ev: &self, count: 0}
        }
}

pub struct Window {
    w: WINDOW,
}

impl Window {
    fn w(&mut self) -> WINDOW { self.w }
}

impl Drop for Window {
    fn drop(&mut self) {
        delwin(self.w);
    }
}


pub struct FrontState {
    sectors: Sectors,
    pages: PageBundle,
}

impl FrontState {

    fn parent_dir_page<T:Debug+DirCache>(win_size: usize, state: &State<T>) -> Page {
         if let Some(pdir) = state.get_parent_dir() {
            Page::from(win_size, pdir.copy_content())
        }
        else {
            Page::empty(win_size)
        }
    }

    fn current_dir_page<T:Debug+DirCache>(win_size: usize, state: &State<T>) -> Page {
        if let Some(content) = state.current_dir().content() {
            Page::from(win_size, content.clone())
        }
        else {
            Page::empty(win_size)
        }
    }

    pub fn from<T:Debug+DirCache>(sectors: Sectors, state: &State<T>) -> Self {

        let [_, rsize, _, msize, _, lsize] = sectors.current_shape();

        let lwin = lsize.0 as usize;
        let mwin = msize.0 as usize;

        let left_page = FrontState::parent_dir_page(lwin, state);

        let mid_page = FrontState::current_dir_page(mwin, state);

        let pages = PageBundle::from(left_page, mid_page, Page::new());
        Self {sectors, pages}
    }

    pub fn soft_refresh(&mut self) {
        self.sectors.soft_refresh();
    }

    pub fn resize_l(&mut self, delta: i32) {
        self.sectors.resize_l(delta);
    }

    pub fn resize_r(&mut self, delta: i32) {
        self.sectors.resize_r(delta);
    }

    pub fn draw_mid<T: DirCache>(&mut self, state: &State<T>) {
        self.pages.update_state(state);
        for (i, entry) in self.pages.mid.view().iter().enumerate() {
                self.sectors.mid.write_text(i as i32 + 1i32, &entry.name(), COLOR_PAIR(CPAIR_WHI));
        }
    }

    pub fn draw_left<T: DirCache>(&mut self, state: &State<T>) {
        self.pages.update_state(state);
        self.sectors.left.clean();
        for (i, entry) in self.pages.left.view().iter().enumerate() {

            if self.pages.left.is_focus(i) {
                self.sectors.left.write_text(i as i32 + 1i32, &entry.name(), COLOR_PAIR(CPAIR_RED));
            }
            else {
                self.sectors.left.write_text(i as i32 + 1i32, &entry.name(), COLOR_PAIR(CPAIR_WHI));
            }

        }
    }

    pub fn draw<T: DirCache>(&mut self, state: &State<T>, n: usize) {
        self.pages.update_state(state);
        self.sectors.get_mut(n).clean();
        for (i, entry) in self.pages.get(n).view().iter().enumerate() {

            let Entry(ftype, _direntry) = entry;

            let cpair = match ftype {
                FType::Dir => (CPAIR_BLU, CPAIR_IBLU),
                FType::Symlink => (CPAIR_GRE, CPAIR_IGRE),
                FType::File => (CPAIR_WHI, CPAIR_IWHI),
                // _ => panic!(),
            };

            if self.pages.get(n).is_focus(i) {
                self.sectors.get_mut(n).write_text(i as i32 + 1i32, &entry.name(), COLOR_PAIR(cpair.1));
            }
            else {
                self.sectors.get_mut(n).write_text(i as i32 + 1i32, &entry.name(), COLOR_PAIR(cpair.0));
            }

        }
    }

    pub fn update_bundle<T: DirCache>(&mut self, state: &State<T>) {

        let [_, rsize, _, msize, _, lsize] = self.sectors.current_shape();

        let lwin = lsize.0 as usize;
        let mwin = msize.0 as usize;

        let left_page = if let Some(pdir) = state.get_parent_dir() {
            Page::from(lwin, pdir.copy_content())
        }
        else {
            Page::empty(lwin)
        };

        let mid_page = if let Some(content) = state.current_dir().content() {
            Page::from(mwin, content.clone())
        }
        else {
            Page::empty(mwin)
        };

        self.pages = PageBundle::from(left_page, mid_page, Page::new());
    }

    pub fn focus_path(&mut self, lmr: usize) -> PathBuf {
        match lmr {
            0 => self.pages.left.focus_path(),
            1 => self.pages.mid.focus_path(),
            2 => self.pages.right.focus_path(),
            _ => panic!()
        }
    }

    pub fn mv_focus(&mut self, lmr: usize, step: i32) {
        match lmr {
            0 => self.pages.left.mv_focus(step),
            1 => self.pages.mid.mv_focus(step),
            2 => self.pages.right.mv_focus(step),
            _ => ()
        }
    }

    pub fn scroll(&mut self, lmr: usize, step: usize, sign: i8) {
        match lmr {
            0 => self.pages.left.scroll(step, sign),
            1 => self.pages.mid.scroll(step, sign),
            2 => self.pages.right.scroll(step, sign),
            _ => ()
        }
    }

}

pub struct PageBundle {
    left: Page,
    mid: Page,
    right: Page
}

impl PageBundle {

    pub fn from(left: Page, mid: Page, right: Page) -> Self {
        Self {left, mid, right}
    }

    pub fn update_state<T: DirCache>(&mut self, state: &State<T>) {
    }

    pub fn get<'a>(&'a self, n: usize) -> &'a Page {
        match n {
            0 => &self.left,
            1 => &self.mid,
            2 => &self.right,
            _ => panic!()
        }
    }

    pub fn get_mut<'a>(&'a mut self, n: usize) -> &'a mut Page {
        match n {
            0 => &mut self.left,
            1 => &mut self.mid,
            2 => &mut self.right,
            _ => panic!()
        }
    }


}

pub struct Page {
    win: usize, // the writable length of the window (constant) (size minus borders)
    cont: Vec<Entry>, // all the entries in the directory
    start: usize, // the start index of cont to show
    end: usize, // the end index of cont to show
    focus: usize, // marks the current cursor
}

impl Page {

    pub fn new() -> Self {
        Self {
            win: 0,
            start: 0,
            end: 0,
            focus: 0,
            cont: Vec::new()
        }
    }

    pub fn from(win_size: usize, cont: Vec<Entry>) -> Self {
        let end = {
            if cont.len() > win_size { win_size - 2 }
            else { cont.len() - 1 }
        };
        Self {
            win: win_size-2,
            start: 0,
            end,
            focus: 0,
            cont
        }
    }

    pub fn empty(win_size: usize) -> Self {
        Self {
            win: win_size,
            start: 0,
            end: 0,
            focus: 0,
            cont: Vec::new()
        }
    }

    pub fn view(&self) -> &[Entry] {
        &self.cont[self.start..self.end]
    }

    pub fn is_focus(&self, n: usize) -> bool {
        if n == self.focus { true } else { false }
    }

    pub fn focus_path(&self) -> PathBuf {
        self.cont[self.focus].1.path()
    }

    pub fn scroll(&mut self, step: usize, sign: i8) {
        if sign < 0 {
            self.scroll_up(step)
        }
        else {
            self.scroll_down(step)
        }
    }

    fn scroll_up(&mut self, step: usize) {
        if self.cont.len() > self.win {
            if self.start as i32 - step as i32 <= 0 {
                self.start =  0;
                self.end = {
                    if self.cont.len() > self.win { self.win }
                    else { self.cont.len() - 1 }
                };
            }
            else {
                self.start -= step;
                self.end -= step;
            }
        }
    }

    fn scroll_down(&mut self, step: usize) {
        if self.cont.len() > self.win {
            if self.end + step >= self.cont.len() - 1 {
                self.end = self.cont.len()-1;
                self.start =  self.end - self.win;
            }
            else {
                self.end += step;
                self.start += step;
            }
        }
    }

    pub fn mv_focus(&mut self, step: i32) {
        if self.focus as i32 + step >= self.end as i32 {
            self.focus = self.end-1;
        }
        else if self.focus as i32 + step <= self.start as i32 {
            self.focus = self.start;
        }
        else {
            self.focus = (self.focus as i32 + step) as usize
        }
    }

}

pub struct Sectors {
    left: SimpleWindow,
    mid: SimpleWindow,
    right: SimpleWindow,
    shape: [YX;6],
    prop: [f32;2],
}

impl Sectors {

    fn calc_proportions(lprop: f32, mprop: f32) -> [YX;6] {

        let origin = YX(0,0);
        let YX(H, W) = screen_size();
        let effscr = W-1;
        let effscr_f32 = effscr as f32;

        // let rprop = 1.0f32 - lprop - mprop;
        let (dL, dM) = ((lprop*effscr_f32) as i32, (mprop*effscr_f32) as i32);
        let dR: i32 = effscr - dL - dM;

        let left_ori = origin + YX(1,0);
        let mid_ori = left_ori + YX(0,dL);
        let right_ori = mid_ori + YX(0,dM);

        let left_size = YX(H-2, dL + 0i32);
        let mid_size = YX(H-2, dM + 0i32);
        let right_size = YX(H-2, dR + 0i32);

        [left_ori, left_size, mid_ori, mid_size, right_ori, right_size]

    }

    pub fn init(lprop: f32, mprop: f32) -> Sectors {

        let shape = Self::calc_proportions(lprop, mprop);

        let [left_ori, left_size, mid_ori, mid_size, right_ori, right_size] = shape;

        let left = SimpleWindow::init(left_ori, left_size);
        let mid = SimpleWindow::init(mid_ori, mid_size);
        let right = SimpleWindow::init(right_ori, right_size);

        Sectors {
            left: left,
            mid: mid,
            right: right,
            shape: shape,
            prop: [lprop,mprop],
        }
    }

    pub fn prop(&self) -> &[f32;2] {
        &self.prop
    }

    pub fn current_shape(&self) -> [YX;6] {
        self.shape
    }

    pub fn left(&self) -> &SimpleWindow {
        &self.left
    }

    pub fn mid(&self) -> &SimpleWindow {
        &self.mid
    }

    pub fn right(&self) -> &SimpleWindow {
        &self.right
    }

    pub fn clear(&mut self) {
        self.left.wclear();
        self.mid.wclear();
        self.right.wclear();
    }

    pub fn hard_refresh(&mut self) {
        self.clear();

        self.left.box_(0 as chtype, 0 as chtype);
        self.mid.box_(0 as chtype, 0 as chtype);
        self.right.box_(0 as chtype, 0 as chtype);

        self.left.wnoutrefresh();
        self.mid.wnoutrefresh();
        self.right.wnoutrefresh();

        doupdate();
    }

    pub fn soft_refresh(&mut self) {
        self.left.wnoutrefresh();
        self.mid.wnoutrefresh();
        self.right.wnoutrefresh();
        doupdate();
    }

    pub fn mid_refresh(&mut self) {
        self.mid.wclear();
        self.mid.box_(0 as chtype, 0 as chtype);
        self.mid.wnoutrefresh();
        doupdate();
    }

    pub fn resize_l(&mut self, delta: i32) {
        let [lori, lsize, mori, msize, rori, rsize] = self.shape;

        let mut new_lsize = lsize+YX(0,delta);
        let mut new_mori = mori+YX(0,delta);
        let mut new_msize = msize-YX(0,delta);

        if new_mori.1 + 2 >= rori.1 {
            new_lsize = YX(lsize.0,rori.1-2);
            new_mori = rori + YX(0,-2);
            new_msize = YX(msize.0,2);
        }
        else if new_lsize.1 <= 2 {
            new_lsize = YX(lsize.0,2);
            new_mori = lori + YX(0,2);
            new_msize = YX(msize.0, rori.1-2);
        }

        let new_shape = [lori, new_lsize, new_mori, new_msize, rori, rsize];
        let tsize = (lsize+msize+rsize).1;

        self.shape = new_shape;
        self.prop = [new_shape[1].1 as f32 / tsize as f32, new_shape[3].1 as f32 / tsize as f32];
        self.left.reshape(new_shape[0], new_shape[1]);
        self.mid.reshape(new_shape[2], new_shape[3]);
        self.right.reshape(new_shape[4], new_shape[5]);
    }

    pub fn resize_r(&mut self, delta: i32) {
        let [lori, lsize, mori, msize, rori, rsize] = self.shape;

        let mut new_msize = msize+YX(0, delta);
        let mut new_rori = rori+YX(0, delta);
        let mut new_rsize = rsize-YX(0, delta);

        if new_rsize.1 <= 1 || new_msize.1 <= 1 {
            new_msize = msize;
            new_rori = rori;
            new_rsize = rsize;
        }

        let new_shape = [lori, lsize, mori, new_msize, new_rori, new_rsize];
        let tsize = (lsize+msize+rsize).1;

        self.left.reshape(new_shape[0], new_shape[1]);
        self.mid.reshape(new_shape[2], new_shape[3]);
        self.right.reshape(new_shape[4], new_shape[5]);
        self.shape = new_shape;
        self.prop = [new_shape[1].1 as f32 / tsize as f32, new_shape[3].1 as f32 / tsize as f32];
    }

    pub fn test_mid(&mut self) {
        for i in 1..10 {
            self.mid.write_text(i as i32, &format!("This is text {}", COLOR_PAIR(i as i16)), COLOR_PAIR(i as i16));
        }
    }

    pub fn write_mid(&mut self, text: &str) {
        self.mid.write_text(1i32, text, COLOR_PAIR(CPAIR_CUST));
    }

    pub fn draw_mid<DC: DirCache+Debug>(&mut self, state: &mut State<DC>) {
        let cdir = state.current_dir();
        if let Some(content) = cdir.content() {
        }
    }

    pub fn draw_left<DC: DirCache+Debug>(&mut self, state: &mut State<DC>) {
        let cdir = state.get_parent_dir().unwrap();
        if let Some(content) = cdir.content() {
            for (i, entry) in content.iter().enumerate() {
                self.left.write_text(i as i32 + 1i32, &entry.name(), COLOR_PAIR(CPAIR_WHI));
            }
        }
    }

    pub fn get_mut<'a>(&'a mut self, n: usize) -> &'a mut SimpleWindow {
        match n {
            0 => &mut self.left,
            1 => &mut self.mid,
            2 => &mut self.right,
            _ => panic!()
        }
    }

}

pub struct SimpleWindow {
    w: Window,
    pos: YX,
    size: YX,
}

impl SimpleWindow {

    pub fn init(pos: YX, size: YX) -> SimpleWindow {
        let mut w = Window { w: newwin(size.0, size.1, pos.0, pos.1) };
        box_(w.w(), 0, 0);

        SimpleWindow {
            w,
            pos,
            size,
        }
    }

    pub fn reshape(&mut self, pos: YX, size: YX) {
        self.pos = pos;
        self.size = size;
        // We clear the borders
        self.box_(NCURSES_ACS(' '), NCURSES_ACS(' '));
        // Resize them
        self.wresize(size.0, size.1);
        // Move them
        self.mvwin(pos.0, pos.1);
        // Write them again
        self.box_(0, 0);
    }

    pub fn write_text(&mut self, i: i32, text: &str, color: NCURSES_ATTR_T) {

        // Aux data
        let yx0 = self.pos;// + YX(0i32, 2i32);
        let yxS = self.size;// + YX(0i32, 2i32);

        // truncate text
        let truncname = truncate(&text.to_string(), yxS.1 as usize -2);

        // Write directories
        self.wattron(color);
        self.mvwprintw(YX(i,1), &truncname);
        self.wattroff(color);
        // self.mvwprintw(YX(1,1), &format!("{:?}", yx0));
    }

    pub fn clean(&mut self) {

        // Aux data
        let yx_size = self.size;// + YX(0i32, 2i32);

        // Construct empty line
        let mut string = String::new();
        for _i in 1..yx_size.1-1 {
            string.push_str(" ")
        }

        // Write every line
        for i in 1..yx_size.0-1 {
            self.mvwprintw(YX(i,1), &string);
        }

    }


}

impl NcursesWindow for SimpleWindow {
    fn window(&mut self) -> WINDOW { self.w.w() }
    fn pos(&mut self) -> YX { self.pos }
    fn size(&mut self) -> YX { self.size }
}

pub trait NcursesWindow {
    fn window(&mut self) -> WINDOW;
    fn pos(&mut self) -> YX;
    fn size(&mut self) -> YX;

    fn box_(&mut self, att1: chtype, att2: chtype) {
        box_(self.window(), att1, att2);
    }

    fn wclear(&mut self) {
        wclear(self.window());
    }

    fn wrefresh(&mut self) {
        wrefresh(self.window());
    }

    fn wnoutrefresh(&mut self) {
        wnoutrefresh(self.window());
    }

    fn redrawwin(&mut self) {
        redrawwin(self.window());
    }

    fn whline(&mut self, ch: chtype, n: i32) {
        whline(self.window(), ch, n);
        // wrefresh(self.window());
    }

    fn wvline(&mut self, ch: chtype, n: i32) {
        wvline(self.window(), ch, n);
        // wrefresh(self.window());
    }

    fn mvwhline(&mut self, yx: YX, ch: chtype, n: i32) {
        let YX(y,x) = yx;
        mvwhline(self.window(), y,x,ch,n);
        // wrefresh(self.window());
    }

    fn mvwvline(&mut self, yx: YX, ch: chtype, n: i32) {
        let YX(y,x) = yx;
        mvwvline(self.window(), y, x, ch, n);
        // wrefresh(self.window());
    }

    fn mvhline(yx: YX, ch: chtype, n: i32) {
        let YX(y,x) = yx;
        mvhline(y,x,ch,n);
    }

    fn mvvline(yx: YX, ch: chtype, n: i32) {
        let YX(y,x) = yx;
        mvvline(y,x,ch,n);
    }

    fn wprintw(&mut self, s: &str) -> i32 {
        wprintw(self.window(), s)
    }

    fn mvwprintw(&mut self, yx: YX, s: &str) -> i32 {
        let YX(y,x) = yx;
        mvwprintw(self.window(), y, x, s)
    }

    fn split_vline(&mut self, x: i32) {
        let YX(h,w) = self.size();
        let YX(y0,x0) = self.pos();
        self.mvwvline(YX(0,x0+x), ACS_TTEE(), 1);
        self.mvwvline(YX(1,x0+x), 0, h-2);
        self.mvwvline(YX(h-1,x0+x), ACS_BTEE(), 1);
    }

    fn split_hline(&mut self, y: i32) {
        let YX(h,w) = self.size();
        let YX(y0,x0) = self.pos();
        self.mvwhline(YX(y0+y,0), ACS_LTEE(), 1);
        self.mvwhline(YX(y0+y,1), 0, w-2);
        self.mvwhline(YX(y0+y, w-1), ACS_RTEE(), 1);
    }

    fn wattron(&mut self, color_pair: NCURSES_ATTR_T) {
        wattron(self.window(), color_pair);
    }

    fn wattroff(&mut self, color_pair: NCURSES_ATTR_T) {
        wattroff(self.window(), color_pair);
    }

    // fn wattr_set(&mut self, attr: NCURSES_ATTR_T, cpair: NCURSES_ATTR_T) {
    //     wattr_set(self.window(), attr, cpair);
    // }

    fn wresize(&mut self, lines: i32, cols: i32) {
        wresize(self.window(), lines, cols);
    }

    fn mvwin(&mut self, lines: i32, cols: i32) {
        mvwin(self.window(), lines, cols);
    }


}

struct Style {
    ls: String,
    rs: String,
    ts: String,
    bs: String,
    tlc: String,
    trc: String,
    blc: String,
    brc: String,
}
