use ncurses::*;
use std::char;
use std::str::{FromStr};
use std::fmt::Debug;

pub mod structures;
pub mod colors;

use front::structures::{*};
use front::structures::NcursesWindow;
// use front::colors;
// use front::colors::{*, COLOR_PAIR_DEFAULT};

use back::{*};

use interaction::{*, Key};

pub type DrawResult = Result<(),Error>;

pub enum Error {
    CleanExit
}

// impl Error {
//     pub fn new() -> Error {
//         Error{}
//     }
// }

pub fn start_ncurses_mode() {
    initscr();
    raw();
    keypad(stdscr(), true);
    noecho();
    curs_set(CURSOR_VISIBILITY::CURSOR_INVISIBLE);
    if has_colors() == true {
        mvprintw(0,0,&format!("COLOR"));
        start_color();
        colors::init_color_set();
    }
}

fn create_entries<'a,T:'a+Debug+DirCache>(function: fn(&'a State<'a,T>) -> Option<&'a Dir>,
                                          state: &'a State<T>,
                                          height: i32) -> Option<EntryView> {
    /* Creates an Option<EntryView> object,
     * */

    let opt_dir = function(state);

    if let Some(dir) = opt_dir {
        let entries = dir.content().unwrap();
        let entry_view = EntryView::new(entries, 0, height as usize -2 );
        Some(entry_view)
    }
    else {
        None
    }
}

fn create_updir_entries<'a,T:'a+Debug+DirCache>(state: &'a State<T>, height: i32) -> Option<EntryView> {

    let opt_pdir = state.get_parent_dir();

    if let Some(pdir) = opt_pdir {
        let entries = pdir.content().unwrap();
        let entry_view = EntryView::new(entries, 0, height as usize -2 );
        Some(entry_view)
    }
    else {
        None
    }
}


pub fn draw_interface<T:Debug+DirCache>(state: &mut State<T>, fstate: &mut FrontState) -> DrawResult {

    if let Some(cwd) = state.current() {
        if let Some(ref mut s) = cwd.to_str() {
            mvprintw(0,0,&format!("{} {:?}\n",s, screen_size()));
        }
        else{
            printw("Can't parse dir name");
        }
    }

    interact(state, fstate)

}

pub fn start_interface<T:Debug+DirCache>(state: &mut State<T>) -> DrawResult {

    // let (lprop, mprop, rprop) = (0.20f32, 0.40f32, 0.40f32);
    let (lprop, mprop) = (0.30f32, 0.30f32);
    let sectors = Sectors::init(lprop, mprop);

    let mut fstate: FrontState = FrontState::from(sectors, state);

    while draw_interface(state, &mut fstate).is_ok() { }

    Err(Error::CleanExit)

}

pub fn end_ncurses_mode() {
    endwin();
}

/*
pub fn test_ncurses() {

    initscr();/* Start curses mode */
    raw();/* Line buffering disabled*/
    keypad(stdscr(), true);/* We get F1, F2 etc..*/
    noecho();/* Don't echo() while we do getch */

    printw("Type any character to see it in bold\n");
    let ch = getch();               /* If raw() hadn't been called
                                    * we have to press enter before it
                                    * gets to the program */
    let mut max_x = 0;
    let mut max_y = 0;
    getmaxyx(stdscr(), &mut max_y, &mut max_x);

    let coords = YX::from_u(0,0);
    let size = screen_size();

    let win = MainWindow::new();
    // coords, size

    if ch == KEY_F1 {             /* Without keypad enabled this will */
        printw("F1 Key pressed");   /*  not get to us either*/
    }
                                    /* Without noecho() some ugly escape
                                     * charachters might have been printed
                                     * on screen*/
    else {
        printw("The pressed key is ");
        attron(A_BOLD());
        printw(
            format!("{}\n", char::from_u32(ch as u32).expect("Invalid char")).as_ref()
            );
        attroff(A_BOLD());
    }
    refresh();/* Print it on to the real screen */
    getch();/* Wait for user input */
    endwin();/* End curses mode  */

}
*/
