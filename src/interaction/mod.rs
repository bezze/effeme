use ncurses::getch;
use ncurses::*;
use std::collections::HashMap;
use std::fs;
use std::path;
use std::char::{self};
use std::str::FromStr;
use std::fmt::Debug;

use back::{*, Mode, State};
use front::{*}; //DrawResult;
use front::structures::{*};

mod vars {
    use std::env;
    pub fn HOME() -> String {
        env::var("HOME").unwrap()
    }
} /* vars */

#[derive(Debug)]
#[derive(PartialEq)]
pub struct Key(pub char);

impl Key {

    pub fn parse(ch: i32) -> Key {
        char::from_u32(ch as u32)
            .map(|c| Key(c) )
            .unwrap()
    }

    pub fn from_str(s: &str) -> Key {
        Key(s.chars().nth(0).unwrap())
    }

    pub fn str2char(s: &str) -> char {
        s.chars().nth(0).unwrap()
    }

    pub fn str2i32(s: &str) -> i32 {
        s.chars().nth(0).unwrap() as i32
    }

    pub fn i32tochar(i: i32) -> char {
        char::from_u32(i as u32).unwrap()
    }

    pub fn i32to_string(i: i32) -> String {
        let c: char = char::from_u32(i as u32).unwrap();
        char::to_string(&c)
    }

    pub fn to_letters(s: &'static str) -> Letters {
        Letters(Key::from_str(s), s)
    }

}

pub struct Letters(Key, &'static str);


pub fn interact<T: Debug+DirCache>(state: &mut State<T>, fstate: &mut FrontState) -> DrawResult {

    refresh();
    fstate.draw(state, 0);
    fstate.draw(state, 1);
    fstate.soft_refresh();
    let ch = getch();
    let letter = Key::parse(ch);
    // let letter = Key::to_letters("q");

    // match letter {
    //     Letters(_, "q")  => Err(Error::CleanExit),
    //     Letters(_, _)  => Ok(())
    // }

    if letter == Key::from_str("q") {
        Err(Error::CleanExit)
    }
    else if letter == Key::from_str("t") {
        fstate.resize_r(-1i32);
        let YX(height, width) = screen_size() - YX(2,0);
        // mvprintw(height+1i32,width-100i32, &format!("{:?}\n",fstate.current_shape()));
        Ok(())
    }
    else if letter == Key::from_str("y") {
        fstate.resize_r(1i32);
        let YX(height, width) = screen_size() - YX(2,0);
        // mvprintw(height+1i32,width-100i32, &format!("{:?}\n",fstate.current_shape()));
        Ok(())
    }
    else if letter == Key::from_str("e") {
        fstate.resize_l(-1i32);
        let YX(height, width) = screen_size() - YX(2,0);
        // mvprintw(height+1i32,width-100i32, &format!("{:?}\n",fstate.current_shape()));
        Ok(())
    }
    else if letter == Key::from_str("r") {
        fstate.resize_l(1i32);
        let YX(height, width) = screen_size() - YX(2,0);
        // mvprintw(height+1i32,width-100i32, &format!("{:?}\n",fstate.current_shape()));
        Ok(())
    }
    else if letter == Key::from_str("d") {
        Ok(())
    }
    else if letter == Key::from_str("h") {
        state.up_scan();
        fstate.update_bundle(state);
        Ok(())
    }
    else if letter == Key::from_str("l") {
        let focus_dir = fstate.focus_path(1);
        state.cd_scan(&focus_dir);
        fstate.update_bundle(state);
        Ok(())
    }
    else if letter == Key::from_str("v") {
        fstate.scroll(0, 5, 1);
        Ok(())
    }
    else if letter == Key::from_str("b") {
        fstate.scroll(0, 5, -1);
        Ok(())
    }
    else if letter == Key::from_str("u") {
        Ok(())
    }
    else if letter == Key::from_str("i") {
        Ok(())
    }
    else if letter == Key::from_str("k") {
        fstate.mv_focus(1, 1);
        // println!("{:?}", state.parent());
        // state.move_down_currdir();
        Ok(())
    }
    else if letter == Key::from_str("j") {
        fstate.mv_focus(1, -1);
        // println!("{:?}", state.parent());
        // state.move_up_currdir();
        Ok(())
    }
    else {
        let YX(height, width) = screen_size() - YX(2,0);
        mvprintw(height+1i32,width-20i32, &format!("{:?} {:?}\n",ch, Key::i32to_string(ch)));
        Ok(())
    }


    // if letter == Key::from_str("q") {
    //     Err(Error::CleanExit)
    // }
    // else if letter == Key::from_str("t") {
    //     fstate.resize_r(-1i32);
    //     let YX(height, width) = screen_size() - YX(2,0);
    //     // mvprintw(height+1i32,width-100i32, &format!("{:?}\n",fstate.current_shape()));
    //     Ok(())
    // }
    // else if letter == Key::from_str("y") {
    //     fstate.resize_r(1i32);
    //     let YX(height, width) = screen_size() - YX(2,0);
    //     // mvprintw(height+1i32,width-100i32, &format!("{:?}\n",fstate.current_shape()));
    //     Ok(())
    // }
    // else if letter == Key::from_str("e") {
    //     fstate.resize_l(-1i32);
    //     let YX(height, width) = screen_size() - YX(2,0);
    //     // mvprintw(height+1i32,width-100i32, &format!("{:?}\n",fstate.current_shape()));
    //     Ok(())
    // }
    // else if letter == Key::from_str("r") {
    //     fstate.resize_l(1i32);
    //     let YX(height, width) = screen_size() - YX(2,0);
    //     // mvprintw(height+1i32,width-100i32, &format!("{:?}\n",fstate.current_shape()));
    //     Ok(())
    // }
    // else if letter == Key::from_str("d") {
    //     Ok(())
    // }
    // else if letter == Key::from_str("h") {
    //     state.up_scan();
    //     fstate.update_bundle(state);
    //     Ok(())
    // }
    // else if letter == Key::from_str("l") {
    //     let focus_dir = fstate.focus_path(1);
    //     state.cd_scan(&focus_dir);
    //     fstate.update_bundle(state);
    //     Ok(())
    // }
    // else if letter == Key::from_str("v") {
    //     fstate.scroll(0, 5, 1);
    //     Ok(())
    // }
    // else if letter == Key::from_str("b") {
    //     fstate.scroll(0, 5, -1);
    //     Ok(())
    // }
    // else if letter == Key::from_str("u") {
    //     Ok(())
    // }
    // else if letter == Key::from_str("i") {
    //     Ok(())
    // }
    // else if letter == Key::from_str("k") {
    //     fstate.mv_focus(1, 1);
    //     // println!("{:?}", state.parent());
    //     // state.move_down_currdir();
    //     Ok(())
    // }
    // else if letter == Key::from_str("j") {
    //     fstate.mv_focus(1, -1);
    //     // println!("{:?}", state.parent());
    //     // state.move_up_currdir();
    //     Ok(())
    // }
    // else {
    //     let YX(height, width) = screen_size() - YX(2,0);
    //     mvprintw(height+1i32,width-20i32, &format!("{}  {:?}\n",ch, letter));
    //     Ok(())
    // }

}


#[derive(Debug)]
pub enum Fun {
    F1, F2, F3, F4,
    F5, F6, F7, F8,
    F9, F10, F11, F12,
}

impl Fun {
    pub fn from(ch: i32) -> Option<Fun> {
        match ch {
            KEY_F1 => Some(Fun::F1),
            KEY_F2  => Some(Fun::F2),
            KEY_F3  => Some(Fun::F3),
            KEY_F4  => Some(Fun::F4),
            KEY_F5  => Some(Fun::F5),
            KEY_F6  => Some(Fun::F6),
            KEY_F7  => Some(Fun::F7),
            KEY_F8  => Some(Fun::F8),
            KEY_F9  => Some(Fun::F9),
            KEY_F10 => Some(Fun::F10),
            KEY_F11 => Some(Fun::F11),
            KEY_F12 => Some(Fun::F12),
                _ => None
        }
    }
}

#[derive(Debug)]
pub enum Mod {
    Ctrl,
    Shift,
    Alt,
    Super
}

#[derive(Debug)]
pub enum Direction {
    Up,
    Down,
    Left,
    Right
}

#[derive(Debug)]
pub enum Symbol {
}

pub struct Mapping{
    mode: Mode,
    keyseq: Vec<String>,
    action: Action
}

impl Mapping {
    pub fn new(mode: Mode, keyseq: Vec<String>, action: Action) -> Mapping {
        Mapping {
            mode,
            keyseq,
            action
        }
    }
}

pub enum Action {
    Move(Direction),
    Copy_,
    Cut,
    Paste,
    Rename,
}

fn parse_command_file() -> HashMap<String,String> {

    let home = vars::HOME();
    let home_path = path::Path::new(&home);
    let config_path = home_path.join(".config/effeme");
    let file_text = fs::read_to_string(config_path.join("effeme.conf"))
        .expect("Can't read file")
        .to_string();

    let mut command : HashMap<_,_> = HashMap::new();

    let lines = file_text.lines(); // splits &str into newlines
    for mut line in lines {

        let collection : Vec<&str> = line.split(" ").collect();
        let mut coll = collection.to_vec();
        command.insert(coll[0].to_string(),coll[1].to_string());
    };

    command
}
