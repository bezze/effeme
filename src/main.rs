extern crate ncurses;
extern crate unicode_segmentation;
extern crate core;

use std::env;
use std::fs;

mod back;
mod front;
mod interaction;

use back::{
    Dir,
    Entry,
    State,
    DirCacheHash,
    DirCache
};

use front::Error;


fn main() {

    let mut dircache = DirCacheHash::new();
    let mut state : State<_> = State::new(&mut dircache);

    front::start_ncurses_mode();
    // while front::start_interface(&mut state).is_ok() { }
    {
    while front::start_interface(&mut state).is_ok() { }
    };
    front::end_ncurses_mode();

}
