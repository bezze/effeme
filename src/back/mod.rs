use std::env;
use std::fs;
use std::path::{PathBuf, Path};
use std::collections::{self,HashMap};
use std::collections::hash_map::Keys;
use std::cell::RefCell;
use std::cell::Ref;
use std::boxed::Box;
use std::fmt::Debug;
use core::clone::Clone;


use std::io;
use std::ffi::{OsString,OsStr};

type ioResult<T> = io::Result<T>;

use unicode_segmentation::UnicodeSegmentation;

// #[derive(Debug)]
// pub enum Entry {
//     Dir(fs::DirEntry),
//     File(fs::DirEntry),
//     Symlink(fs::DirEntry)
// }

#[derive(Clone,Debug)]
pub enum FType {
    Dir,
    File,
    Symlink
}

#[derive(Clone,Debug)]
pub struct DirEntry {
    path: PathBuf,
    filename: OsString,
    metadata: fs::Metadata
}

impl DirEntry {
    pub fn from_fs_direntry(de: fs::DirEntry) -> DirEntry {
        // TODO: THIS IS QUICK'N'DIRTY, we should handle this error
        let meta = de.metadata().unwrap();
        DirEntry {
            path: de.path(),
            filename: de.file_name(),
            metadata: meta
        }
    }

    pub fn file_name(&self) -> OsString {
        self.filename.clone()
    }

    pub fn path(&self) -> PathBuf {
        self.path.clone()
    }

    pub fn file_type(&self) {
    }

    pub fn is_dir(&self) {
    }

    pub fn is_file(&self) {
    }

    pub fn len(&self) {
    }

}

#[derive(Clone,Debug)]
pub struct Entry (pub FType, pub DirEntry);

impl Entry {

    pub fn from_direntry(direntry: fs::DirEntry) -> Entry {

        let de = DirEntry::from_fs_direntry(direntry);

        let m =  fs::symlink_metadata(de.path()).unwrap();
        let ft = m.file_type();

        if ft.is_dir() {
            Entry(FType::Dir, de)
        } else if ft.is_file() {
            Entry(FType::File, de)
        } else {
            Entry(FType::Symlink, de)
        }

    }

    pub fn name(&self) -> String {

        fn unwrap_name(de: &DirEntry) -> String {
            let name = de.file_name().into_string();
            match name {
                Ok(n) => n,
                Err(e) => String::from("Can't parse non utf-8 name"),
            }
        }

        unwrap_name(&self.1)
    }

    pub fn trunc_name(&self, width: i32) -> String {
        fn truncate(string : &String, limit : usize) -> String {
            let title_str = &string[..];
            let title_vec = UnicodeSegmentation::graphemes(title_str, true).collect::<Vec<&str>>();
            if title_vec.len() >= limit {
                let mut trunc = title_vec[..(limit-1)].join("");
                trunc.push_str("~");
                trunc
            }
            else {
                title_vec.join("")
            }
        }
        truncate(&self.name(), width as usize)
    }

}



#[derive(Debug)]
pub struct Dir {
    path: PathBuf,
    content: Option<Vec<Entry>>,
    parent: Option<PathBuf>,
}

impl Dir {
    pub fn new() -> Dir {
        let dir : PathBuf = env::current_dir().unwrap();
        let parent = dir.clone().parent().map(|x| x.to_owned());
        Dir {
            content: Some(dir.read_dir()
                .unwrap()
                .map( |e| Entry::from_direntry(e.unwrap()))
                .collect()),
            path: dir,
            parent: parent,
        }
    }

    pub fn from_path(p: &PathBuf) -> Dir {
        let contents = fs::read_dir(p);
        let parent = p.clone().parent().map(|x| x.to_owned());
        Dir {
            path: p.to_owned(),
            content: match contents {
                Ok(c) => Some(c.map( |e| { Entry::from_direntry(e.unwrap()) }) .collect()),
                Err(e) => None,
            },
            parent: parent,
        }
    }

    pub fn name<'a>(&'a self) -> Option<&'a OsStr> {
        self.path.file_name()
    }

    pub fn parent(&self) -> Option<&Path> {
        self.path.parent()
    }

    pub fn parent_dir(&self) -> Option<Dir> {
        if let Some(parent_path) = self.path.parent() {
            Some(Dir::from_path(&parent_path.to_path_buf()))
        }
        else {
            None
        }
    }

    pub fn content(&self) -> Option<&Vec<Entry>> {
        self.content.as_ref()
    }

    pub fn copy_content(&self) -> Vec<Entry> {
        if let Some(v) = &self.content {
            let mut copy = Vec::new();
            for entry in v {
                copy.push(entry.clone())
            }
            copy
        }
        else {
            Vec::new()
        }
    }


    pub fn content_len(&self) -> Option<usize> {
        self.content().as_ref().map(|e| e.len())
    }
}

impl Clone for Dir {
    fn clone(&self) -> Self { Dir::from_path(&self.path) }
}


pub trait DirCache: Debug {
    fn new() -> Self;
    fn insert(&mut self, path: PathBuf);
    fn populate(&mut self, dir: &PathBuf);
    fn keys(&self) -> Keys<PathBuf,Dir>;
    fn contains_key(&self, k: &PathBuf) -> bool;
    fn get<'a>(&'a self, k: &'a PathBuf) -> Option<&'a Dir>;
}


#[derive(Debug)]
pub struct DirCacheHash {
    pub cache: HashMap<PathBuf,Dir>
}

impl DirCacheHash {
}


impl DirCache for DirCacheHash {
    fn new() -> DirCacheHash {
        DirCacheHash { cache: HashMap::new() }
    }

    fn populate(&mut self, dirpath: &PathBuf) {
        if self.contains_key(dirpath) {
            let dir = {
                let ref dir = self.get(dirpath).unwrap();
                 (*dir).clone()
            };
            if let Some(ref content) = dir.content {
                for entry in content.iter() {
                    if let Entry(FType::Dir, de) = entry {
                        self.insert(de.path());
                    }
                }
            }
        }
        else {
            let new_dir : Dir = Dir::from_path(dirpath);
            self.cache.insert(dirpath.clone(), new_dir);
            self.populate(dirpath);
        }
    }

    fn keys(&self) -> Keys<PathBuf,Dir> {
        self.cache.keys()
    }

    fn contains_key(&self, k: &PathBuf) -> bool {
        self.cache.contains_key(k)
    }


    fn insert(&mut self, path: PathBuf) {
        let d = Dir::from_path(&path);
        self.cache.insert(path, d);
    }

    fn get<'a>(&'a self, k: &'a PathBuf) -> Option<&'a Dir> {
        self.cache.get(k)
    }
}


#[derive(Debug)]
pub enum Mode {
    Visual,
    Normal
}


#[derive(Debug)]
pub struct Focus {
    currdir_len: Option<usize>,
    currdir: Option<usize>,
    updir_len: Option<usize>,
    updir: Option<usize>,
}

impl Focus {
    pub fn currdir(&self) -> Option<&usize> {
        self.currdir.as_ref()
    }

    pub fn updir(&self) -> Option<&usize> {
        self.updir.as_ref()
    }

    fn move_up(dir: &mut Option<usize>) {
        if let Some(n) = dir.take() {
            if n > 0 {
                *dir = Some(n-1);
            }
            else {
                *dir = Some(0);
            }
        }
    }

    fn move_down(dir: &mut Option<usize>, N: usize) {
        if let Some(n) = dir.take() {
            if n < N-1 {
                *dir = Some(n+1);
            }
            else {
                *dir = Some(N-1);
            }
        }
    }

    pub fn move_up_currdir(&mut self) {
        Focus::move_up(&mut self.currdir)
    }

    pub fn move_down_currdir(&mut self) {
        let n = self.currdir_len.unwrap_or(0);
        Focus::move_down(&mut self.currdir, n);
        // printw(&format!("Adding {:?}",self.currdir));
    }

    pub fn move_up_updir(&mut self) {
        Focus::move_up(&mut self.updir)
    }

    pub fn move_down_updir(&mut self) {
        let N = self.updir_len.unwrap_or(0);
        Focus::move_down(&mut self.updir, N)
    }
}


#[derive(Debug)]
pub struct State<'a, T: 'a + DirCache> {
    mode: Mode,
    selected: Vec<Entry>,
    initial: PathBuf,
    current: Option<PathBuf>,
    focus: Focus,
    cache: &'a mut T // RefCell<T>
}

impl<'a, T: 'a+DirCache+Debug> State<'a,T> {
    pub fn new(cache: &'a mut T) -> State<'a,T> {
        // let mut cache = <T as DirCache>::new();
        let initial = env::current_dir().unwrap();

        cache.populate(&initial);


        let focus: Focus = {
            let init_dir = cache.get(&initial);
            let par_dir = init_dir.and_then(|d| d.parent_dir());
            let ilen = init_dir.and_then(|d| d.content_len());
            let plen = par_dir.and_then(|d| d.content_len());
            Focus{
                currdir_len: ilen,
                currdir: Some(0),
                updir_len: plen,
                updir: None
            }
        };

        State {
            mode: Mode::Normal,
            selected: Vec::new(),
            initial: initial.clone(),
            current: Some(initial),
            focus: focus,
            cache: cache
            // cache: RefCell::new(cache)
        }
    }

    pub fn initial(&self) -> &PathBuf {
        &self.initial
    }

    pub fn current(&self) -> Option<&PathBuf> {
        self.current.as_ref()
    }

    pub fn focus(&self) -> &Focus {
        &self.focus
    }

    pub fn update_focus(&mut self) {
        self.focus = {
            let init_dir = &self.get_current_dir();
            let par_dir = init_dir.and_then(|d| d.parent_dir());
            let ilen = init_dir.and_then(|d| d.content_len());
            let plen = par_dir.and_then(|d| d.content_len());
            Focus{
                currdir_len: ilen,
                currdir: Some(0),
                updir_len: plen,
                updir: None
            }
        };

    }


    pub fn current_dir(&self) -> &Dir {
        //TODO: Warning on this unwrap
        self.current()
            .and_then(|p| self.get_dir(p))
            .unwrap()
    }

    pub fn cache_keys(&self) -> Keys<PathBuf,Dir> {
        <T as DirCache>::keys(self.cache)
        // Ref::map(self.cache.borrow(), |mi| <T as DirCache>::keys(&mi))
    }

    pub fn cache_contains_key(&self, path: &PathBuf) -> bool {
        self.cache.contains_key(path)
    }

    pub fn get_dir<'b>(&'b self, path: &'b PathBuf) -> Option<&'b Dir> {
        self.cache.get(path)
    }

    pub fn scan_current(&mut self) {
        if let Some(dir) = &self.current {
            self.cache.populate(dir);
        }
    }

    pub fn get_current_dir(&self) -> Option<&Dir> {
        self.current()
            .and_then(|currdir| self.get_dir(&currdir))
    }

    pub fn get_parent_dir(&self) -> Option<Dir> {
        self.get_current_dir().and_then(|r_dir| r_dir.parent_dir())
    }

    pub fn is_currdir_empty(&self) -> bool {
        let currdir: &Dir = self.current_dir();
        currdir.content_len().map_or(true, |n| if n == 0 { true } else { false })
    }

    pub fn is_pardir_empty(&self) -> bool {
        let pardir: Option<Dir> = self.current_dir().parent_dir(); //.map(|d| d.parent_dir());
        pardir.and_then(|d| d.content_len())
            .map_or(true, |n| 
                    if n == 0 { true } else { false }
            )
    }

    pub fn up(&mut self) {
        if let Some(current) = self.current.take() {
            if let Some(parent) = current.parent() {
                self.current = Some(parent.to_path_buf());
            }
        }
    }

    pub fn ups(&mut self, n: usize) {
        for _i in 1..n {
            self.up()
        }
    }

    pub fn up_scan(&mut self) {
        self.up();
        self.scan_current();
        self.update_focus();
    }

    pub fn down(&mut self) {
        if let Some(current) = self.current.take() {
            if let Some(n) = self.focus.currdir() {
                self.current = Some(current)
            }
        }
    }

    pub fn down_scan(&mut self) {
        self.down();
        self.scan_current();
        self.update_focus();
    }


    pub fn cd(&mut self, dest_path: &PathBuf) {

        if let Some(current) = self.current.take() {
            self.current = Some(dest_path.clone());
        }

        // if self.current.is_some() {
        //     let fcdir = if self.is_currdir_empty() { None } else { Some(0) };
        //     let fudir = if self.is_pardir_empty() { None } else { Some(0) };
        //     self.focus = Focus {currdir: fcdir, updir: fudir};
        // }

    }

    pub fn cd_scan(&mut self, dest_path: &PathBuf) {
        self.cd(dest_path);
        self.scan_current();
        self.update_focus();
    }

    pub fn move_up_currdir(&mut self) {
        self.focus.move_up_currdir()
    }

    pub fn move_down_currdir(&mut self) {
        self.focus.move_down_currdir()
    }

    pub fn move_up_updir(&mut self) {
        self.focus.move_up_updir()
    }
    pub fn move_down_updir(&mut self) {
        self.focus.move_down_updir()
    }

    pub fn status(&self) {
        println!("Mode = {:?}", self.mode);
        println!("Selected = {:?}", self.selected);
        println!("Initial = {:?}", self.initial);
        println!("Current = {:?}", self.current);
        println!("Cache Keys := {{");
        // for key in <T as DirCache>::keys(&self.cache.borrow()) {
        for key in self.cache_keys() {
            println!("     {:?}", key);
        }
        println!("}}");
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use super::{DirCacheHash};
    // use std::path::{PathBuf, Path};
    // use std::env;

    mod fixture {
        use super::*;
        pub fn nav_dir() -> PathBuf {
            let mut cd_dir = PathBuf::from(env!("CARGO_MANIFEST_DIR"));
            cd_dir.push("tests/nav_tests");
            cd_dir
        }
    }

    #[test]
    fn test_initial_dir() {
        let mut dircache = DirCacheHash::new();
        let mut state : State<_> = State::new(&mut dircache);
        let initial : PathBuf = env::current_dir().unwrap();
        assert_eq!(state.initial(), &initial);
    }

    #[test]
    fn test_currdir_empty() {
        let mut dircache = DirCacheHash::new();
        let mut state : State<_> = State::new(&mut dircache);
        let initial : PathBuf = env::current_dir().unwrap();
        assert_eq!(state.initial(), &initial);

        let cd_dir = fixture::nav_dir();
        // let fcdir = if state.is_currdir_empty() { None } else { Some(0) };
        assert_eq!(state.is_currdir_empty(), false)
    }

    #[test]
    fn test_change_dir() {
        let mut dircache = DirCacheHash::new();
        let mut state : State<_> = State::new(&mut dircache);
        let initial : PathBuf = env::current_dir().unwrap();
        assert_eq!(state.initial(), &initial);

        let cd_dir = fixture::nav_dir();
        state.cd(&cd_dir);
        let current = state.current().unwrap();

        assert_eq!(current, &cd_dir);

    }

    #[test]
    fn test_up() {
        let mut dircache = DirCacheHash::new();
        let mut state : State<_> = State::new(&mut dircache);
        let mut cd_dir = fixture::nav_dir();
        cd_dir.push("dir2/dir21");
        let mut up_dir = fixture::nav_dir();
        up_dir.push("dir2");

        state.cd(&cd_dir);
        state.up();

        assert_eq!(&state.current.unwrap(), &up_dir)
    }

    #[test]
    fn test_ups() {
        let mut dircache = DirCacheHash::new();
        let mut state : State<_> = State::new(&mut dircache);

        let mut cd_dir = fixture::nav_dir();
        cd_dir.push("dir2/dir21/dir212");
        let up_dir = fixture::nav_dir();

        state.cd(&cd_dir);
        state.ups(4);

        assert_eq!(&state.current, &Some(up_dir))
    }

    #[test]
    fn test_scan_current() {
        let mut dircache = DirCacheHash::new();
        let mut state : State<_> = State::new(&mut dircache);

        let cd_dir = fixture::nav_dir();

        let mut dir1 = fixture::nav_dir(); dir1.push("dir1");
        let mut dir2 = fixture::nav_dir(); dir2.push("dir2");
        let mut dir3 = fixture::nav_dir(); dir3.push("dir3");
        let mut not_there = fixture::nav_dir(); not_there.push("dir2/dir21");

        state.cd(&cd_dir);
        state.scan_current();

        assert!(state.cache_contains_key(&dir1));
        assert!(state.cache_contains_key(&dir2));
        assert!(state.cache_contains_key(&dir3));
        assert_eq!(state.cache_contains_key(&not_there), false)
    }

    #[test]
    fn test_up_scan() {
        let mut dircache = DirCacheHash::new();
        let mut state : State<_> = State::new(&mut dircache);
        let mut cd_dir = fixture::nav_dir(); cd_dir.push("dir1/dir13");
        let mut up_dir = fixture::nav_dir(); up_dir.push("dir1");

        state.cd(&cd_dir);

        assert_eq!(&state.current, &Some(cd_dir));

        state.up_scan();

        let mut dir11 = fixture::nav_dir(); dir11.push("dir1/dir11");
        let mut dir12 = fixture::nav_dir(); dir12.push("dir1/dir12");
        let mut dir13 = fixture::nav_dir(); dir13.push("dir1/dir13");
        let mut dir14 = fixture::nav_dir(); dir14.push("dir1/dir14");
        let mut not_there = fixture::nav_dir(); not_there.push("dir2/dir21");

        assert!(state.cache_contains_key(&dir11));
        assert!(state.cache_contains_key(&dir12));
        assert!(state.cache_contains_key(&dir13));
        assert!(state.cache_contains_key(&dir14));
        assert_eq!(state.cache_contains_key(&not_there), false);
        assert_eq!(&state.current, &Some(up_dir));
    }
}
